import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TasksService } from './services/tasks.service';
import { TasksController } from './controllers/tasks.controller';
import { Task } from './entities/task.entity';
import { Equipos } from './entities/Equipos.entity';
import { actividades } from './entities/actividades.entity';
import { actividadesService } from './services/actividades.service';
import { EquiposService } from './services/Equipos.service';
import { EquiposController } from './controllers/Equipos.controller';
import { actividadesController } from './controllers/actividades.controller';


@Module({
  imports: [
    TypeOrmModule.forFeature([Task,Equipos,actividades])
  ],
  providers: [TasksService,EquiposService,actividadesService],
  controllers: [TasksController, EquiposController, actividadesController]
})
export class TasksModule {}
