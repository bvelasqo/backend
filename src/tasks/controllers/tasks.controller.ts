import { Controller, Get, Param, Post, Body, Put , Delete} from '@nestjs/common';
import { appendFile } from 'fs';
import { AppModule } from 'src/app.module';
import { Task } from '../entities/task.entity';
import { TasksService } from './../services/tasks.service';


@Controller('api/tasks')
export class TasksController {
    
    constructor(
        private tasksService: TasksService
      ) {}

    /*@Get()
    getAll(){
        return ["Balanza Digital 35000 g","Pesa Individual de 1000 g ","Horno de Secado","Termohigrómetro Digital","Cronometro"];
    }*/

    @Get()
    findAll() {
        return this.tasksService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: number) {
        return this.tasksService.findOne(id);
    }

    @Post()
    create(@Body() body: any) {
    
        return this.tasksService.create(body);
    }

    @Put(':id')
    update(@Param('id') id: number, @Body() body: any) {
        return this.tasksService.update(id, body);
    }

    @Delete(':id')
    delete(@Param('id') id: number) {
        return this.tasksService.remove(id);
    }
}
